# Submission Details

### Frameworks/Libraries Used

    - express
    - express-validator
    - helmet
    - typescript

### Assumptions Made

1. The endpoint should utilise a GET verb
2. The endpoint will be consumed by clients making separate requests for each user wishing to be checked.
3. The endpoint does not need to support an array of users being passed in as query params
4. If a user's location is in "includedCountries", but _also_ in "exludedCountries", the exclusion takes precedent
5. Authentication and Authorisation logic is not a requirement of this task
6. Serving the API locally over HTTP is OK for this task

### Setup / Test / Run Submission

1. Clone this repo down

2. Change directory into cloned project and run npm install and npm run build

```
$ cd /path/to/ab-testing-tool
$ npm i
$ npm run build
```

3. To run the tests, written in Jest, run npm test

I have only written 5 basic tests to cover a range of input/output scenarios based on the example_users.json file

```
$ npm test
```

4.  To start the API, run npm start

```
$ npm start
```

4. To consume the endpoint, either go to a browser or use cURL to make a GET request

```
$ curl -kv http://localhost:3000/flags?email=SOME_EMAIL_ADDRESS&location=SOME_LOCATION
```

### Things to note

1. Idempotent request functionality is provided by storing previous requests in memory as a hashmap, so any node restarts would wipe it out. 
