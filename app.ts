import express, { Request, Response, NextFunction } from 'express'
import createError from 'http-errors'
import helmet from 'helmet'
import indexRouter from './routes/index'

const app = express()

app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(helmet())

/**
 * Route Definitions
 */
app.use('/', indexRouter)

// catch 404 and forward to error handler
app.use((_req: Request, _res: Response, next: NextFunction) => {
  next(createError(404))
})

/**
 * Error Handling
 */
app.use((err: any, req: Request, res: Response) => {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render('error')
})

module.exports = app
