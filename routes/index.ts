import { Request, Response, Router } from 'express'
import { query } from 'express-validator'
import { getEnabledFeatures, IFeature, IFeatureFlagsResponse } from '../source/feature-flags'
import { checkValidationResult } from '../utils/validator'

const router = Router()

/**
 * GET /flags 
 * 
 * Generates list of the features that are enabled for given user’s email and location
 * 
 * Uses express-validator to validate and sanitise query params
 */
router.get('/flags', [
  query('email', 'Please provide a valid email address').normalizeEmail().isEmail(),
  query('location', 'Please provide a valid location').isLength({ min: 2 }).trim().escape(),
  checkValidationResult
], async (req: Request, res: Response) => {
  // Prepare the response object, using validated and sanitised query params
  const response: IFeatureFlagsResponse = {
    email: req.query.email as string,
    location: req.query.location as string,
    enabledFeatures: []
  }
  // Grab the list of features enabled for the given email and location
  const enabledFeatures = await getEnabledFeatures(response.email, response.location)
    .catch(err => {
      res.status(500).json({
        message: 'Something went wrong, please try again!'
      })
      console.error(err)
      return 
    }) as IFeature[]

  response.enabledFeatures = enabledFeatures 
  
  res.status(200).json({ flags: response })
})

export default router
