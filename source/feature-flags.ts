import { readFileSync } from 'fs'

const features = JSON.parse(readFileSync('./features.json').toString())
const processedUsers = new Map()

/**
 * Responsible for aggregating a list of enabled features for the provided email or location
 * 
 * @param email
 * @param location 
 */
 export async function getEnabledFeatures (email: string, location: string): Promise<IFeature[]> {
  const userKey = email + ':' + location
  if (processedUsers.has(userKey)) {
    return processedUsers.get(userKey).enabled
  }

  processedUsers.set(userKey, { enabled: [] })

  for (let feature of features) {
      const emailEnabled = feature.enabledEmails.includes(email)
      const countryEnabled = (feature.includedCountries.length === 0 || feature.includedCountries.includes(location)) && !feature.excludedCountries.includes(location)
      const ratioEnabled = feature.ratio > 0 && (Math.random() < feature.ratio)

      if (emailEnabled || (countryEnabled && ratioEnabled)) {
          processedUsers.get(userKey).enabled.push({ name: feature.name })
      }
  }
  return processedUsers.get(userKey).enabled
}

/**
 * Interface defining a Feature that is enabled for a given user
 */
export interface IFeature {
  name: string
}
  
/**
 * Interface defining the expected response payload
 */
export interface IFeatureFlagsResponse {
  email: string
  location: string
  enabledFeatures: IFeature[]
}