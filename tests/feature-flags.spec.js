const FeatureFlags = require('../source/feature-flags')

describe('Feature flag tests...', () => {
    it('Should return MarketingBanner, EnhancedDashboardFeature (50%), NewUserOnboardingJourney (25%)', async () => {
        const features = await FeatureFlags.getEnabledFeatures('sandy@example.com', 'US')

        expect(features.length).toBeGreaterThanOrEqual(1)
        expect(features.length).toBeLessThanOrEqual(3)
        expect(features[0]).toMatchObject({ name: 'MarketingBanner' })

        if (features[1]) {
            expect(['EnhancedDashboardFeature', 'NewUserOnboardingJourney']).toContain(features[1].name)
        }
        if (features[2]) {
            expect(features[2]).toMatchObject({ name: 'NewUserOnboardingJourney' })
        }
    })

    it('Should return SuperCoolFeature, MarketingBanner, SimplifiedNavBar, EnhancedDashboardFeature (50%), NewUserOnboardingJourney (25%)', async () => {
        const features = await FeatureFlags.getEnabledFeatures('fred@example.com', 'US')

        expect(features.length).toBeGreaterThanOrEqual(3)
        expect(features.length).toBeLessThanOrEqual(5)

        expect(features[0]).toMatchObject({ name: 'SuperCoolFeature' })
        expect(features[1]).toMatchObject({ name: 'MarketingBanner' })
        expect(features[2]).toMatchObject({ name: 'SimplifiedNavBar' })

        if (features[3]) {
            expect(['EnhancedDashboardFeature', 'NewUserOnboardingJourney']).toContain(features[3].name)
        }
        if (features[4]) {
            expect(features[4]).toMatchObject({ name: 'NewUserOnboardingJourney' })
        }
    })

    it('Should return NewUserOnboardingJourney (25%)', async () => {
        const features = await FeatureFlags.getEnabledFeatures('adam@example.com', 'CH')
        expect(features.length).toBeGreaterThanOrEqual(0)
        expect(features.length).toBeLessThanOrEqual(1)

        if (features[0]) {
            expect(features[0]).toMatchObject({ name: 'NewUserOnboardingJourney' })
        }
    })

    it('Should return SuperCoolFeature', async () => {
        const features = await FeatureFlags.getEnabledFeatures('mike@example.com', 'GB')
        expect(features.length).toBe(1)
        expect(features[0]).toMatchObject({ name: 'SuperCoolFeature' })
    })

    it('Should return idempotent response for a given email and location combination.', async () => {
        const firstRequest = await FeatureFlags.getEnabledFeatures('sandy@example.com', 'US')
        const secondRequest = await FeatureFlags.getEnabledFeatures('sandy@example.com', 'US')
        const thirdRequest = await FeatureFlags.getEnabledFeatures('sandy@example.com', 'US')

        expect(firstRequest.length).toBe(secondRequest.length)
        expect(secondRequest.length).toBe(thirdRequest.length)
        expect(thirdRequest.length).toBe(firstRequest.length)
    })
})