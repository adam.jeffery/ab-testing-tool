import { validationResult } from 'express-validator'
import { NextFunction, Request, Response } from 'express'

/**
 * Responsible for checking whether an express-validator pipeline has validated successfully or not
 * 
 * @param req 
 * @param res 
 * @param next 
 * @returns
 */
 export function checkValidationResult(req: Request, res: Response, next: NextFunction) {
    const result = validationResult(req)
    if (result.isEmpty()) {
        return next()
    }
  
    res.status(422).json({ errors: result.array() })
  }